class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :set_locale
 
  def set_locale
    if params[:locale]
      I18n.locale = params[:locale]
      cookies[:locale] = params[:locale]
    elsif cookies[:locale]
      I18n.locale = cookies[:locale]
    end
  end

  def authenticate
    if Rails.env.production?
      authenticate_or_request_with_http_basic do |username, password|
        username == "pantarul" && password == "pan4aru8?"
      end 
    end
  end
end
