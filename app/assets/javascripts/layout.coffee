sizes = {
  logo_start: 150,
  logo_end: 292,
  logo_diff: ->
    this.logo_end - this.logo_start
  ,
  nav_padding: 220,
  fixed_padding: 334
}

scroll = ->
  return if $(window).width() < 768
  scroll_top = $(window).scrollTop()
  if(scroll_top < 180)
    $("nav").css('padding-top', sizes.nav_padding - scroll_top/180 * (sizes.nav_padding - 40))
    $("img.logo-image").css({
      "width": sizes.logo_end - scroll_top/180*(sizes.logo_diff())
      "margin-top": 60 - scroll_top/180*40
    })
  else
    $("nav").css('padding-top', 40)
    $("img.logo-image").css({
      "width": sizes.logo_start,
      "margin-top": "20px"
    })


resize = ->
  width = $(window).width()
  return if width < 768
  width = 1200 if(width) > 1200
  width = 768 if (width) < 768
  sizes.logo_end = 292 - (1200 - width)/432*100
  sizes.nav_padding = 220 - (1200 - width)/432*50
  sizes.fixed_padding = 334 - (1200 - width)/432*50
  scroll()

gallery_next = ->
  $current = $('ul.gallery li:visible')
  $next = $current.next()
  return if $next.size() == 0
  if $current.index() == $('ul.gallery li').size() - 2
    $('.gallery-arrows .right-arrow').hide()
  if $current.index() == 0
    $('.gallery-arrows .left-arrow').show()
  $('ul.gallery li:visible').animate({
    left: -500
  }, ->
    $(this).hide()
  )
  $next.css('left', 500).show().animate({
    left: 0
  })

gallery_prev = ->
  $current = $('ul.gallery li:visible')
  $next = $current.prev()
  return if $next.size() == 0
  if $current.index() == 1
    $('.gallery-arrows .left-arrow').hide()
  if $current.index() == $('ul.gallery li').size() - 1
    $('.gallery-arrows .right-arrow').css('display', '')
  $('ul.gallery li:visible').animate({
    left: 500
  }, ->
    $(this).hide()
  )
  $next.css('left', -500).show().animate({
    left: 0
  })


gallery = ->
  $("ul.gallery img").swipeleft(->
    gallery_next()
  )
  $("ul.gallery img").swiperight(->
    gallery_prev()
  )
  $(".gallery-arrows .right-arrow").click(->
    gallery_next()
  )
  $(".gallery-arrows .left-arrow").click(->
    gallery_prev()
  )
  $('ul.gallery li img').click(->
    return if $(window).width() < 768
    visible_index = $(this).parent().index()
    $("ul.big-gallery li").hide()
    $("ul.big-gallery li:eq(" + $(this).parent().index() + ")").show()
    $(".big-gallery-arrows a").show()
    if(visible_index == 0)
      $(".big-gallery-arrows .left-arrow").hide()
    else if(visible_index == ($("ul.gallery li").size() - 1))
      $(".big-gallery-arrows .right-arrow").hide()
      
    $(".big-gallery-wrap").modal({
      zIndex: 2000,
      opacity: 80
    })
  )
  $(".big-gallery-arrows .right-arrow").click(->
    $current = $('ul.big-gallery li:visible')
    $next = $current.next()
    if $current.index() == $('ul.big-gallery li').size() - 2
      $('.big-gallery-arrows .right-arrow').hide()
    if $current.index() == 0
      $('.big-gallery-arrows .left-arrow').show()
    $('ul.big-gallery li:visible').animate({
      left: -1000
    }, ->
      $(this).hide()
    )
    $next.css('left', 1000).show().animate({
      left: 0
    })
  )
  $(".big-gallery-arrows .left-arrow").click(->
    $current = $('ul.big-gallery li:visible')
    $next = $current.prev()
    if $current.index() == 1
      $('.big-gallery-arrows .left-arrow').hide()
    if $current.index() == $('ul.big-gallery li').size() - 1
      $('.big-gallery-arrows .right-arrow').css('display', '')
    $('ul.big-gallery li:visible').animate({
      left: 1000
    }, ->
      $(this).hide()
    )
    $next.css('left', -1000).show().animate({
      left: 0
    })
  )
  $("ul.gallery-pager li").click(->
    index = $(this).index()
    $("ul.gallery li").hide()
    $("ul.gallery li:eq(" + index + ")").show()
    $(".gallery-arrows .left-arrow").css('display', 'inline-block')
    $(".gallery-arrows .right-arrow").css('display', '')
    if(index == 0)
      $(".gallery-arrows .left-arrow").hide()
    else if(index == ($("ul.gallery li").size() - 1))
      $(".gallery-arrows .right-arrow").hide()
  )
  

resize()
$ ->
  console.log("ready")
  resize()
  $(window).resize($.throttle(20, resize))
  scroll()
  $(window).scroll($.throttle(20, scroll))
  gallery()
  $(".menu").click(->
    $(".menu").toggleClass("open")
    $(".mobile-menu nav").slideToggle()
  )
  $(".mobile-menu nav ul li a").click(->
    $(".mobile-menu nav").slideUp()
    $(".menu").removeClass("open")
  )
  $(".maps-iframe-target").append('<iframe width="600" height="480" frameborder="0" style="border:0;pointer-events:none" src="https://www.google.com/maps/embed/v1/place?q=Ulica%20kralja%20Tomislava%201%2C%20Dubrovnik%2C%20Croatia&key=AIzaSyCADXrX3qGBNWXnuOXntY_-1tcMw5PB5vU"></iframe>')
