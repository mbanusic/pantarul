# config valid only for Capistrano 3.1
lock '3.4.0'
require 'capistrano/bundler'

set :application, 'Pantarul'
set :repo_url, 'git@bitbucket.org:mbanusic/pantarul.git'
set :tmp_dir, "/home/pantarul/tmp"

set :deploy_via, :remote_cache
set :use_sudo, false

# Default branch is :master
# ask :branch, proc { `git rev-parse --abbrev-ref HEAD`.chomp }.call

# Default deploy_to directory is /var/www/my_app
set :deploy_to, '/home/pantarul/cap'

# Default value for :scm is :git
# set :scm, :git

# Default value for :format is :pretty
# set :format, :pretty

# Default value for :log_level is :debug
# set :log_level, :debug

# Default value for :pty is false
set :pty, true

# Default value for :linked_files is []
# set :linked_files, %w{config/database.yml}

# Default value for linked_dirs is []
set :linked_dirs, %w{log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system}

# Default value for default_env is {}
set :default_env, { path: "/home/Pantarul/ruby/bin:$PATH" }

# Default value for keep_releases is 5
# set :keep_releases, 5
#

set :keep_releases, 3
#after "deploy:update", "deploy:cleanup"

namespace :deploy do

  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      #run "touch #{File.join(current_path,'tmp','restart.txt')}"
      # Your restart mechanism here, for example:
      execute :touch, release_path.join('tmp/restart.txt')
    end
  end

  after :publishing, :restart

  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      # Here we can do anything such as:
      # within release_path do
      #   execute :rake, 'cache:clear'
      # end
    end
  end

end
